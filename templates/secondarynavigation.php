<div class="logo">
    <p><img class="center-block" alt="Site logo" src="../images/userprofile.png" /></p>
    <p class="text-center color-white">Hi Admin</p>
</div>

<div class="nav">
  <ul class="nav nav-stacked">

    <li role="presentation" class="active second-background">
        <a href="../dashboard"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Dashboard</a>
    </li>

    <li role="presentation" class="second-background">
        <a href="../user"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View User</a>
    </li>

    <li role="presentation" class="active second-background">
        <a href="../menu"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Menu Category</a>
    </li>

    <li role="presentation" class="active second-background">
        <a href="../menuitem"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Menu Item</a>
    </li>

    <li role="presentation" class="active second-background">
        <a href="../order"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Order</a>
    </li>

    <li role="presentation" class="active second-background">
        <a href="../aboutus"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> About Us</a>
    </li>

    <li role="presentation" class="second-background">
        <a href="logout"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Logout</a>
    </li>

    </ul>
</div>
